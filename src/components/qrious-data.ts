import {Vue} from 'vue-property-decorator';

export default interface QriousData {
    element: Vue | Element | Vue[] | Element[];
    background: string;
    backgroundAlpha: number;
    foreground: string;
    foregroundAlpha: number;
    level: string;
    mime: string;
    padding: number;
    size: number;
    value: string;
}
