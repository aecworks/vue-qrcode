# vue-qrcode
使用vue cli 3 封装的qrcode组件

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## How to use
### install
```
npm install --save @aecworks/vue-qrcode
```

### Generate a qr code
```
<template>
    <div id="app">
        <span>这是使用二维码组件的example</span>
        <qriously :value="qrCodeContent"></qriously>
    </div>
</template>

<script>
    import Qriously from '@aecworks/vue-qrcode';

    export default {
        name: 'app',
        data() {
            return {
                qrCodeContent:
                    'http://www.aecworks.cn',
            };
        },
        components: {
            Qriously,
        },
    };
</script>

<style>
    #app {
        text-align: center;
        }
</style>
```

### Keywords
```
vue-qrcode
```